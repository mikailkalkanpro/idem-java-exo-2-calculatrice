<!doctype html>
<html lang="fr">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <title>Hello, world!</title>
</head>
<body>
<h3>Fromulaire</h3>
<div>
    <div class="container">

        <form action="${pageContext.request.contextPath}/calculatrice" method="post">
            <div class="row justify-content-center bg-dark text-white p-3">

                <div class="col-12 m-5">
                    <fieldset class="">
                        <input type="hidden" name="field_calc" id="field_calc"  value="${calcul}"/>
                        <div class="form-control">${calcul}</div>
                    </fieldset>

                </div>
                <div class="col-3">
                    <input type="submit" name="calcButton" value="1"  class="w-100 form-control" />
                </div>
                <div class="col-3">
                    <input type="submit" name="calcButton" value="2" class="w-100  form-control"/>
                </div>

                <div class="col-3">
                    <input type="submit" name="calcButton" value="3" class="w-100  form-control"/>
                </div>

                <div class="col-3">
                    <input type="submit" name="calcButton" value="+" class="w-100  form-control"/>
                </div>

                <div class="col-3">
                    <input type="submit" name="calcButton" value="4" class="w-100  form-control" />
                </div>

                <div class="col-3">
                    <input type="submit" name="calcButton" value="5" class="w-100  form-control"/>
                </div>

                <div class="col-3">
                    <input type="submit" name="calcButton" value="6"  class="w-100  form-control"/>
                </div>

                <div class="col-3">
                    <input type="submit" name="calcButton" value="-" class="w-100  form-control"/>
                </div>

                <div class="col-3">
                    <input type="submit" name="calcButton" value="7"  class="w-100  form-control"/>
                </div>

                <div class="col-3">
                    <input type="submit" name="calcButton" value="8"  class="w-100  form-control"/>
                </div>

                <div class="col-3">
                    <input type="submit" name="calcButton" value="9" class="w-100  form-control"/>
                </div>

                <div class="col-3">
                    <input type="submit" name="calcButton" value="*"  class="w-100  form-control"/>

                </div>

                <div class="col-3">
                    <input type="submit" name="calcButton" value="0" class="w-100 form-control"/>

                </div>

                <div class="col-3">
                    <input type="submit" name="calcButton" value="c" class="w-100 form-control"/>
                </div>

                <div class="col-3">
                    <input type="submit" name="calcButton" value="=" class="w-100 form-control"/>
                </div>

                <div class="col-3">
                    <input type="submit" name="calcButton" value="/"  class="w-100 form-control"/>
                </div>

            </div>

        </form>


    </div>

</div>

<!-- Optional JavaScript; choose one of the two! -->

<!-- Option 1: Bootstrap Bundle with Popper -->
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>

<!-- Option 2: Separate Popper and Bootstrap JS -->
<!--
<script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js" integrity="sha384-IQsoLXl5PILFhosVNubq5LC7Qb9DXgDA9i+tQ8Zj3iwWAwPtgFTxbJ8NT4GN1R8p" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.min.js" integrity="sha384-cVKIPhGWiC2Al4u+LWgxfKTRIcfu0JTxR+EQDz/bgldoEyl4H0zUF0QKbrJ0EcQF" crossorigin="anonymous"></script>
-->
</body>
</html>