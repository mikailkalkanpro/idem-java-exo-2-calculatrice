<!doctype html>
<%@ page contentType="text/html;charset=utf-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Liste des DVD</title>
    <jsp:include page="link.jsp" />
</head>
<body>

<div class="container mt-5">
    <div class="jumbotron">
        <a class="float-right" href="index.jsp">Home</a>
        <h2>Liste des DVD</h2>
    </div>
</div>

<table class="table table-stripe">
    <thead>
    <tr>
        <td>Id</td>
        <td>Titre</td>
        <td>Duree</td>
        <td>Genre</td>
        <td>Rate</td>
    </tr>
    </thead>

    <tbody>
    <c:forEach items="${listPersonne}" var="person">
        <tr>
            <td>${person.id}</td>
            <td>${person.nom}</td>
            <td>${person.prenom}</td>
            <td>${person.email}</td>
            <td>${person.password}</td>
        </tr>
    </c:forEach>
    </tbody>
</table>

</body>
</html>