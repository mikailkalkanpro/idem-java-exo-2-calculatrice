<!doctype html>
<%@ page contentType="text/html;charset=utf-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Liste des Bien</title>
    <jsp:include page="link.jsp" />
</head>
<body>

<div class="container mt-5">
    <div class="jumbotron">
        <a class="float-right" href="index.jsp">Home</a>
        <h2>Liste des Bien</h2>
    </div>
    <div>
        <a href="ajoutForm.jsp" class="btn btn-default btn-success float-right">Create Bien</a>
    </div>


    <table class="table table-stripe mt-5">
        <thead>
        <tr>
            <td>Id</td>
            <td>Name</td>
            <td>Adress</td>
            <td>Image</td>
            <td>Price</td>
            <td>Action</td>
        </tr>
        </thead>

        <tbody>
        <c:forEach items="${listBien}" var="bien">
            <tr>
                <td>${bien.id}</td>
                <td>${bien.name}</td>
                <td>${bien.adress}</td>
                <td>${bien.image}</td>
                <td>${bien.price}</td>
                <td class="d-flex flex-row justify-content-around gap">
                    <form action="modif" method="post">
                        <input type="hidden" name="init" value="modif">
                        <input type="hidden" name="id" value="${dvd.id}">
                        <input type="submit" value="up" class="btn btn-default btn-warning ">
                    </form>
                    <form action="bien" method="post">
                        <input type="hidden" name="init" value="supr">
                        <input type="hidden" name="id" value="${bien.id}">
                        <input type="submit" value="del" class="btn btn-default btn-danger ">
                    </form>
                </td>
            </tr>
        </c:forEach>
        </tbody>
    </table>
</div>

</body>
</html>