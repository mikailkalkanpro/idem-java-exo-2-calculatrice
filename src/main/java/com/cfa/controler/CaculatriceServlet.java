package com.cfa.controler;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet(name = "CaculatriceServlet", urlPatterns = {"/calculatrice"})
public class CaculatriceServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        this.doRequest(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        this.doRequest(req, resp);
    }

    private void doRequest(HttpServletRequest req,HttpServletResponse resp) throws ServletException, IOException{

        resp.setContentType("text/html;charset=utf-8");
        PrintWriter out = resp.getWriter();



        StringBuilder res = new StringBuilder();
        res.append("<html><body>");

        String calcul = req.getParameter("calcButton"); //init button
        String field_calc = req.getParameter("field_calc"); //init button
        System.out.println(req.getMethod());
        RequestDispatcher rdisp = null;

        if (req.getMethod().equalsIgnoreCase("GET")){
            rdisp =req.getRequestDispatcher("index.jsp");

            String testRes = req.getParameter("button_calc_1");
            System.out.println(testRes);

            req.setAttribute("calcul", "le resultat est : " + testRes);
            req.setAttribute("calcButton", "le calcul est : " + testRes);
        }else {//cas post

            rdisp = req.getRequestDispatcher("calculatrice.jsp");

            if (field_calc.equals("")){
                req.setAttribute("calcul", calcul);
            }else {
                field_calc = req.getParameter("field_calc"); //init button
                req.setAttribute("calcul", field_calc + calcul);

                if (calcul.equals("=")){
                    System.out.println("passer dans le egale");
                    String [] resultat =  field_calc.split("(?<=\\d)(?=\\D)|(?<=\\D)(?=\\d)");


                    float finalResult = Float.parseFloat(resultat[0]);


                    for (int i = 0; i < resultat.length; i++) {

                        switch (resultat[i]){

                            case "+":
                                finalResult += Float.parseFloat(resultat[i+1]);
                                break;
                            case "-":
                                finalResult -= Float.parseFloat(resultat[i+1]);
                                break;
                            case "/":
                                finalResult /= Float.parseFloat(resultat[i+1]);
                                break;
                            case "*":
                                finalResult *= Float.parseFloat(resultat[i+1]);
                                break;
                        }

                    }

                    System.out.println("finalResult:" + finalResult);
                    req.setAttribute("calcul", finalResult);


                }

                if(calcul.equals("c")){
                    // Removing first and last character
                    // of a string using substring() method
                    field_calc = field_calc.substring(0, field_calc.length() - 1);
                    req.setAttribute("calcul", field_calc);
                }
            }
        }
        rdisp.forward(req,resp);


        res.append("</body></html>");

        out.println(res.toString());
    }


}
