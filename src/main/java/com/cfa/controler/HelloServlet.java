package com.cfa.controler;

import com.cfa.dto.Personne;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet(name = "helloServlet", urlPatterns = {"/hello"})
public class HelloServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {
        resp.setContentType("text/html;charset=utf-8"); //ser a forcer si errreur dans l'html pas bien mis html htpl (oublie de ferme) une balise
        PrintWriter out = resp.getWriter();

        Personne personne =
                new Personne("Kalkan","Mikail","mikail@pro","1234");
        Personne personne2 =
                new Personne("Durand","Elodie","durand@mail","1234");

        StringBuilder res = new StringBuilder();
        res.append(personne.toString()+ "<br />");
        res.append(personne2.toString()+ "<br />");

        out.println("<!doctype html>\n" +
                "<html lang=\"en\">\n" +
                "  <head>\n" +
                "    <!-- Required meta tags -->\n" +
                "    <meta charset=\"utf-8\">\n" +
                "    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">\n" +
                "\n" +
                "    <!-- Bootstrap CSS -->\n" +
                "    <link href=\"https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css\" rel=\"stylesheet\" integrity=\"sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC\" crossorigin=\"anonymous\">\n" +
                "\n" +
                "    <title>Hello, world!</title>\n" +
                "  </head>\n" +
                "  <body>\n" +
                "    <nav class=\"navbar navbar-expand-lg navbar-light bg-light\">\n" +
                "  <div class=\"container-fluid\">\n" +
                "    <a class=\"navbar-brand\" href=\"#\">Navbar</a>\n" +
                "    <button class=\"navbar-toggler\" type=\"button\" data-bs-toggle=\"collapse\" data-bs-target=\"#navbarSupportedContent\" aria-controls=\"navbarSupportedContent\" aria-expanded=\"false\" aria-label=\"Toggle navigation\">\n" +
                "      <span class=\"navbar-toggler-icon\"></span>\n" +
                "    </button>\n" +
                "    <div class=\"collapse navbar-collapse\" id=\"navbarSupportedContent\">\n" +
                "      <ul class=\"navbar-nav me-auto mb-2 mb-lg-0\">\n" +
                "        <li class=\"nav-item\">\n" +
                "          <a class=\"nav-link active\" aria-current=\"page\" href=\"#\">Home</a>\n" +
                "        </li>\n" +
                "        <li class=\"nav-item\">\n" +
                "          <a class=\"nav-link\" href=\"#\">Link</a>\n" +
                "        </li>\n" +
                "        <li class=\"nav-item dropdown\">\n" +
                "          <a class=\"nav-link dropdown-toggle\" href=\"#\" id=\"navbarDropdown\" role=\"button\" data-bs-toggle=\"dropdown\" aria-expanded=\"false\">\n" +
                "    Dropdown\n" +
                "            </a>\n" +
                "          <ul class=\"dropdown-menu\" aria-labelledby=\"navbarDropdown\">\n" +
                "            <li><a class=\"dropdown-item\" href=\"#\">Action</a></li>\n" +
                "            <li><a class=\"dropdown-item\" href=\"#\">Another action</a></li>\n" +
                "            <li><hr class=\"dropdown-divider\"></li>\n" +
                "            <li><a class=\"dropdown-item\" href=\"#\">Something else here</a></li>\n" +
                "          </ul>\n" +
                "        </li>\n" +
                "        <li class=\"nav-item\">\n" +
                "          <a class=\"nav-link disabled\" href=\"#\" tabindex=\"-1\" aria-disabled=\"true\">Disabled</a>\n" +
                "        </li>\n" +
                "      </ul>\n" +
                "      <form class=\"d-flex\">\n" +
                "        <input class=\"form-control me-2\" type=\"search\" placeholder=\"Search\" aria-label=\"Search\">\n" +
                "        <button class=\"btn btn-outline-success\" type=\"submit\">Search</button>\n" +
                "      </form>\n" +
                "    </div>\n" +
                "  </div>\n" +
                "</nav>\n" +
                "    <h1>Hello, world!</h1>\n" +
                "\n" +
                    res.toString() +
                "\n" +
                "    <!-- Optional JavaScript; choose one of the two! -->\n" +
                "\n" +
                "    <!-- Option 1: Bootstrap Bundle with Popper -->\n" +
                "    <script src=\"https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js\" integrity=\"sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM\" crossorigin=\"anonymous\"></script>\n" +
                "\n" +
                "    <!-- Option 2: Separate Popper and Bootstrap JS -->\n" +
                "    <!--\n" +
                "    <script src=\"https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js\" integrity=\"sha384-IQsoLXl5PILFhosVNubq5LC7Qb9DXgDA9i+tQ8Zj3iwWAwPtgFTxbJ8NT4GN1R8p\" crossorigin=\"anonymous\"></script>\n" +
                "    <script src=\"https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.min.js\" integrity=\"sha384-cVKIPhGWiC2Al4u+LWgxfKTRIcfu0JTxR+EQDz/bgldoEyl4H0zUF0QKbrJ0EcQF\" crossorigin=\"anonymous\"></script>\n" +
                "            -->\n" +
                "  </body>\n" +
                "</html>");
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("text/html;charset=utf-8");
        HttpSession session = req.getSession();
        PrintWriter out = resp.getWriter();

        StringBuilder res = new StringBuilder();

        Personne personne = (Personne)session.getAttribute("user");
        res.append(personne.getFullName()+ "<br />");
        res.append("<a href='logout'> se deconecter </a>");
        out.println("<htpl><body>" + res.toString() + "</body> </htpl>");
    }
}
