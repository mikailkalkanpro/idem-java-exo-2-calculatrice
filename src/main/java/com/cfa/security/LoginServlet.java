package com.cfa.security;

import com.cfa.dto.Personne;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;
import java.util.Scanner;

@WebServlet(name = "LoginServlet", urlPatterns = {"/login"})
public class LoginServlet extends HttpServlet {
    private String path = "/CSV/user.csv";

    public LoginServlet() throws IOException {
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String email = req.getParameter("email");
        String mdp = req.getParameter("mdp");

        HttpSession session = req.getSession();
        session.setAttribute("connected", "false");
        String realPath = getServletContext().getRealPath(path);

        List<Personne> listuser = this.getListUsers(realPath); //prend la list des utilisateur
        ListIterator<Personne> iterator = listuser.listIterator();


        while (iterator.hasNext()){ //boucle sur les utilisateur
            Personne personne = iterator.next();

            if (personne.getMdp().equals(mdp) && personne.getEmail().equals(email)){ //si il existe
                System.out.println("user existe");
                session.setAttribute("connected", "true");
                session.setAttribute("user", personne);
                break;
            }

        }
        RequestDispatcher rdisp = null;

        if (Boolean.parseBoolean((String) session.getAttribute("connected")) ){
            System.out.println("passer dans le if");
            rdisp = req.getRequestDispatcher("hello");
            rdisp.forward(req,resp);
        }else {
            req.setAttribute("errMsg", "Vous avez pas saisie les bon identifiants");
            rdisp = req.getRequestDispatcher("login.jsp");
            rdisp.forward(req,resp);
        }

    }


    private List<Personne> getListUsers(String path) throws FileNotFoundException{
        List<Personne> personnes = new ArrayList<Personne>();

        FileInputStream fis = new FileInputStream(path); // je vais chercher le fichier

        Scanner sc = new Scanner(fis); // je scane le fichier
        int i = 0;
        while (sc.hasNextLine()){ //boucle chaque ligne csv
            String line = sc.nextLine(); // init line pour chaque ligne
            if (i!=0){ // pas prendre la premir ligne
                line = line.replaceAll("\"","");//remplace des "\" present
                String[] var = line.split(";"); // split la ou il ya ;
                Personne personne = new Personne(var[0],var[1],var[2],var[3]); //parse dans une nouveaux personne a chaque iteration
                personnes.add(personne); // rempli la list avec chaque personne
            }
            i++;
        }


        return personnes;
    };

}
