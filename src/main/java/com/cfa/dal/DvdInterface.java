package com.cfa.dal;

import com.cfa.dto.Dvd;

import java.util.List;

public interface DvdInterface {
    List<Dvd> findAll();
    Dvd findById(Integer id);
    Dvd updateDvd(Integer id, Dvd dvd);
    void addDvd(Dvd dvd);
    void delDvd(Integer id);

}
