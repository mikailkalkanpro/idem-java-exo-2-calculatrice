package com.cfa.dal;


import com.cfa.dto.Bien;
import com.cfa.dto.Dvd;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;
import java.util.ArrayList;
import java.util.List;

public class BienService implements BienInterface {

    EntityManagerFactory factory;
    EntityManager entityManager;


    public BienService() {
        this.factory = Persistence.createEntityManagerFactory("BienDB");
        this.entityManager = factory.createEntityManager();
    }

    @Override
    public List<Dvd> findAll() {
        List<Dvd> listing = new ArrayList<Dvd>();
        try {
            Query query = this.entityManager.createQuery("select d from Dvd d", Dvd.class);
            listing = query.getResultList();
        }catch (Exception e){

        }
        return listing;
    }

    @Override
    public Bien findById(Integer id) {
        Bien bien = new Bien();
        try {
            bien = this.entityManager.find(Bien.class, id);
        }catch (Exception e){
            e.getMessage();
        }
        return bien;
    }

    @Override
    public Bien updateDvd(Integer id, Bien bien) {
        try{
            Bien oldBien = this.entityManager.find(Bien.class, id);
            this.entityManager.getTransaction().begin(); // revien a l'etat anterieur si ya un pb avec entier dans un string etc dans la bdd
            // modifi la table a partir du moment ou l'ensble de la tables est valider
            oldBien.setTitre(bien.getTitre());
            oldBien.setGenre(bien.getGenre());
            oldBien.setRate(bien.getRate());
            oldBien.setDuree(bien.getDuree());


            this.entityManager.getTransaction().commit();
            this.closed();

            return oldBien;
        }catch (Exception e){
            e.getMessage();
        }
        return null;
    }

    private void closed() {
        this.entityManager.close();
        this.factory.close();
    }

    @Override
    public void addBien(Bien bien) {
        try {
            this.entityManager.getTransaction().begin();
            this.entityManager.persist(bien);
            this.entityManager.getTransaction().commit();
            this.closed();
        }catch (Exception e){
            e.getMessage();
        }
    }

    @Override
    public void delBien(Integer id) {
        try {
            Bien bien = this.entityManager.find(Bien.class, id);
            this.entityManager.getTransaction().begin();
            this.entityManager.remove(bien);
            this.entityManager.getTransaction().commit();
            this.closed();
        }catch (Exception e){
            e.getMessage();
        }
    }
}
